<?php
function request_orders( $key, $token){
	global $page, $perpagenum;
	$curl = curl_init();
	curl_setopt_array($curl, array(CURLOPT_URL => "http://in.skyhub.com.br/orders?page=$page&per_page=$perpagenum",
								   CURLOPT_RETURNTRANSFER => true,
								   CURLOPT_ENCODING => "",
								   CURLOPT_MAXREDIRS => 10,
								   CURLOPT_TIMEOUT => 30,
								   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								   CURLOPT_CUSTOMREQUEST => "GET",
								   CURLOPT_HTTPHEADER => array( "accept: application/json",
																"cache-control: no-cache",
																"content-type: application/json",
																$key, 
															   	$token),));
	$json = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) 
	{
		echo "cURL Error #:" . $err;
	} 
	else 
	{
		$getInfoGerais = json_decode($json, true);       
		return $getInfoGerais;
	}
}

function request_unicos( $key, $token, $curlopt_url){
	global $page, $perpagenum;
	$curl = curl_init();
	curl_setopt_array($curl, array(CURLOPT_URL => $curlopt_url,
								   CURLOPT_RETURNTRANSFER => true,
								   CURLOPT_ENCODING => "",
								   CURLOPT_MAXREDIRS => 10,
								   CURLOPT_TIMEOUT => 60,
								   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								   CURLOPT_CUSTOMREQUEST => "GET",
								   CURLOPT_HTTPHEADER => array( "accept: application/json",
																"cache-control: no-cache",
																"content-type: application/json",
																$key, 
															   	$token),));
	$json = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) 
	{
		echo "cURL Error #:" . $err;
	} 
	else 
	{
		$getInfoGerais = json_decode($json, true);       
		return $getInfoGerais;
	}
}


function drop_create($nome_tabela,$tabela,$bd){
    
	$drop_create = "USE [$bd]
					DROP TABLE [$nome_tabela] 			
					CREATE TABLE [$nome_tabela](
					[id] [integer] IDENTITY(1,1) PRIMARY KEY";        
	
	// percorre todas as colunas a serem criadas na tabela
	foreach ($tabela as $col => $lin){
			$nova_linha = ",[$col] [varchar](250) NULL";
			$drop_create .= $nova_linha;
		}
	
	$final_query = ")";
	
	$drop_create .= $final_query;

	echo $drop_create;
	return $drop_create;
}


function query_insert_auto($nome_tabela,$array_dados){
    $insert_nomes = "INSERT INTO [$nome_tabela](";
    
    $insert_valores = ") VALUES (";
    
    
   
    foreach ($array_dados as $coluna => $linha){
        $insert_nomes .= "$coluna,";
        $insert_valores .= "'$linha',";
    }
    
    $insert_nomes = substr($insert_nomes,0, strlen($insert_nomes)-1);
    $insert_valores = substr($insert_valores,0, strlen($insert_valores)-1);
    $query = $insert_nomes.$insert_valores.');';
    
//echo $query;
return $query;
}


function request_stmt_num($sql)
{
    global $db_connect;
    global $params;
    global $options;
    $stmt    = sqlsrv_query($db_connect, $sql, $params, $options);
    $num_sql = sqlsrv_num_rows($stmt);
    //echo "num: " . $num_sql . "<br />";
    
    //var_export($stmt);
    if (!$stmt) {
        $errors = sqlsrv_errors();
        echo "Erro ao buscar pedidos no bd";
        echo $errors;
        if (is_array($errors)) {
            foreach ($errors as $error) {
                echo 'SQLSTATE: ' . $error['SQLSTATE'] . "<br />";
                echo 'code: ' . $error['code'] . "<br />";
                echo 'message: ' . $error['message'] . "<br />";
            }
        } else {
            echo $errors;
        }
    }
    return array(
        "stmt" => $stmt,
        "num_sql" => $num_sql
    );
    
    
}

function link_bd($var_insert){
    global $db_connect, $params, $options;
    //echo '<br>';
    echo 'INICIA LINK_BD::<br>';
    var_dump($var_insert);
    $result_insert_cnt = sqlsrv_query($db_connect, $var_insert);
    if (!$result_insert_cnt) {
        $errors = sqlsrv_errors();
        
        echo "Erro ao modificar o bd";
        echo '<br>';
        echo $errors;
        if (is_array($errors)) {
            foreach ($errors as $error) {
                echo 'SQLSTATE: ' . $error['SQLSTATE'] . "<br />";
                echo 'code: ' . $error['code'] . "<br />";
                echo 'message: ' . $error['message'] . "<br />";
            }
        } else {
            echo $errors;
        }
    }
    else{
        echo "<br>DADOS INSERIDOS COM SUCESSO: <br>";
    }
}

// funcao que retorna id da ultima linha inserida no bd
function sqlsrv_insert_id() {

	global $db_connect, $params, $options;

	$result = sqlsrv_query($db_connect,"SELECT @@identity", $params, $options);
	sqlsrv_fetch($result);

	return sqlsrv_get_field($result, 0);
}



//$tabela_id é a tabela onde estão armazenados os status
//$coluna_status é o nome da coluna que queremos analisar na $tabela_id
//$status_novo é o nome do status que queremos verifiar a existência
//$dado é o nome da coluna que queremos selecionar (ex: 'id', 'host')
//$array_insert é o array que será inserido caso o id ainda não exista
function verifica_insere_dado ($tabela_id,  $coluna_status, $status_novo, $id, $array_insert){
    global $db_connect, $params, $options;
    
    $request_status = "SELECT $id
            FROM $tabela_id
            WHERE $coluna_status = '$status_novo'";
   // echo "coluna status: $coluna_status";
    //echo "status novo: $status_novo";
    //echo "Query: $request_status";
    
    
    $result_status = sqlsrv_query($db_connect, $request_status, $params, $options);
    //echo $result_status;
    //var_export($stmt);
    if (!$result_status) {
        $errors = sqlsrv_errors();
        echo "Erro ao buscar status";
        echo $errors;
        if (is_array($errors)) {
            foreach ($errors as $error) {
                echo 'SQLSTATE: ' . $error['SQLSTATE'] . "<br />";
                echo 'code: ' . $error['code'] . "<br />";
                echo 'message: ' . $error['message'] . "<br />";
            }
        } else {
            echo $errors;
        }
    }
    
    $required_id = sqlsrv_fetch_array($result_status);   
    $required_id = $required_id[$id];
    //echo "required_id: $required_id";
    
    if ($required_id == 0 or $required_id == ''){
        $query_insercao = query_insert_auto($tabela_id, $array_insert);
        link_bd($query_insercao);
        $request_status = "SELECT $id
            FROM $tabela_id
            WHERE $coluna_status = '$status_novo'";
        echo "required id: $required_id";
        var_dump($request_status);
        $result_status = sqlsrv_query($db_connect, $request_status, $params, $options);
        $required_id = sqlsrv_fetch_array($result_status);   
        $required_id = $required_id[$id];
        echo "NOVO ID INSERIDO: $required_id";
        
    }
    
    return $required_id;
}

?>